/* see https://humanstxt.org/ */


/* Web developement */
Author: Nesakko
Email: nesakko [at] tutanota.com
Location: France

/* Site */
Creation date: 2021
Last update: 2024
Standards: HTML5, CSS3, JavaScript
Software: VScode, Firefox
Git repository: https://codeberg.org/Nesakko/MyLinksPage